import sys

args = sys.argv

fout = open(args[2], 'w')
flag = False
for line in open(args[1]):
    if flag:
        fields = line.rstrip().split('\t')[2:]
        for i, f in enumerate(fields):
            fout.write(f +  '\t')
        fout.write('\n')
    flag = True
        
