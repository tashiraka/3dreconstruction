import sys

args = sys.argv
pdbFile = args[1]
idFile = args[2]
outFile = args[3]

pdb = []
for line in open(pdbFile):
    if line[0:6] == 'HETATM' or line[0:22] == 'REMARK 465 MISSING BIN':
        pdb.append(line)

ids = []
for line in open(idFile):
    if line[0] != '#':
        fields = line.rstrip().split("\t")
        ids.append([int(fields[3]), int(fields[4])])

fout = open(outFile, 'w')
for id in ids:
    for i in range(id[0], id[1]+1):
        fout.write(pdb[i])

