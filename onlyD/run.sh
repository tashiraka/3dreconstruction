inFile=$1

python seikei.py ${inFile} ${inFile}.cfm
./checkConnection ${inFile}.cfm ${inFile}.connected ${inFile}.bin.connectivity
./cfm_to_3D ${inFile}.connected 1000 ${inFile}.xyz ${inFile}.eigVal ${inFile}.distMat
./checkGOF ${inFile}.eigVal ${inFile}.xyz ${inFile}.distMat ${inFile}.gof
./xyz_to_pdb ${inFile}.xyz ${inFile}.bin.connectivity chrIntervals.txt ${inFile}.pdb

