import std.stdio, std.conv, std.string, std.array,
  std.algorithm, std.math, std.range;


version(unittest){}
 else {
   void main(string[] args)
   {
     auto EigValFile = args[1];
     auto coordFile = args[2];
     auto distMatFile = args[3];
     auto outFile = args[4];

     auto eigVals = readVec(EigValFile);
     auto coords = readMat(coordFile);
     auto distMat = readMat(distMatFile);

     auto gof = calcGofEigVal(eigVals);
     auto rmsd = calcGofEigVec(coords, distMat);
     auto consensusIndex = calcConsensusIndex(coords, distMat, eigVals);
     
     auto fout = File(outFile, "w");
     fout.writeln("goodness of fit (GOF)");
     fout.writeln("top3 eigenvalues / total eigenvalues: ", gof);
     fout.writeln("RMSD between D and distances from X: ", rmsd);
     fout.writeln("consensus index: ", consensusIndex);
   }
 }


double calcConsensusIndex(double[][] coords, double[][] distMat,
                          double[] eigVals)
{
  auto estimatedDistMat = new double[][](distMat.length, distMat.length);
  foreach(i, row; distMat) 
    foreach(j, elem; row) 
      estimatedDistMat[i][j] = euclideanDist(coords[i], coords[j]);

  auto distRatio = 0.0;
  foreach(i, row; distMat) 
    foreach(j, elem; row)
      if(estimatedDistMat[i][j] != 0 && distMat[i][j] != 0)
        distRatio += min(estimatedDistMat[i][j] / distMat[i][j],
                         distMat[i][j] / estimatedDistMat[i][j]);
  
  if(eigVals.length < 3)
    throw new Exception("ERROR: the number of eigenvalues < 3");
  auto absEigVals = eigVals.map!(x => abs(x));
  auto gof =  absEigVals[$-3..$].sum / absEigVals.sum;

  return distRatio / distMat.length / distMat.length * gof;
}


// assuming eigVals is sorted by ascending order.
double calcGofEigVal(double[] eigVals)
{
  if(eigVals.length < 3)
    throw new Exception("ERROR: the number of eigenvalues < 3");
  auto absEigVals = eigVals.map!(x => abs(x));
  return absEigVals[$-3..$].sum / absEigVals.sum;
}


double calcGofEigVec(double[][] coords, double[][] distMat)
{
  double sqrSum = 0.0;
  
  foreach(i, row; distMat) 
    foreach(j, elem; row) 
      sqrSum += (euclideanDist(coords[i], coords[j]) - distMat[i][j])^^2;

  return sqrSum.sqrt / distMat.length^^2;
}


double euclideanDist(double[] u, double[] v)
{
  return zip(u, v).map!(x => (x[0] - x[1])^^2).sum.sqrt;
}


double[] readVec(string filename)
{
  auto vecApp = appender!(double[]);
  auto fin = File(filename, "r");
  foreach(line; fin.byLine) {
    vecApp.put(line.to!string.strip.to!double);
  }
  return vecApp.data;
}


double[][] readMat(string filename)
{
  auto matApp = appender!(double[][]);
  auto fin = File(filename, "r");
  foreach(line; fin.byLine) {
    matApp.put(line.to!string.strip.split("\t").map!(x => x.to!double).array);
  }
  return matApp.data;
}


void writeMat(double[][] mat, string filename)
{
  auto fout = File(filename, "w");
  foreach(row; mat) {
    foreach(i, elem; row) {
      fout.write(elem);
      if(i < row.length - 1) {
        fout.write("\t");
      }
    }
    fout.writeln;
  }
}
