import std.stdio, std.conv, std.string, std.array, std.algorithm,
  std.math, std.range;


alias long integer;
alias double doublereal;


extern(C) int dsyev_(char *jobz, char *uplo, integer *n, doublereal *a, 
                     integer *lda, doublereal *w, doublereal *work,
                     integer *lwork, integer *info);


void syev(char jobz, char uplo, integer n, doublereal* a, integer lda,
          doublereal *w, doublereal *work, integer lwork, ref integer info)
{
  dsyev_(&jobz, &uplo, &n, a, &lda, w, work, &lwork, &info);
}


version(unittest){}
 else {
   void main(string[] args)
   {
     auto cfmFile = args[1];
     auto scalingFactor = args[2].to!double;
     auto coordFile = args[3];
     auto eigValFile = args[4];
     auto distMatFile = args[5];

     auto cfm = readMat(cfmFile);
     auto distMat = cfmToDistMat(cfm);
     auto gramMat = DtoG(distMat);
     auto eigs = GtoE(gramMat);
     auto coords = EtoX(eigs.eigVals, eigs.eigVecs, scalingFactor);
     writeMat(coords, coordFile);
     writeVec(eigs.eigVals, eigValFile);
     writeMat(distMat, distMatFile);
   }
 }


// Warshall-Floyd Algorithm
double[][] cfmToDistMat(double[][] cfm)
{
  auto distMat = new double[][](cfm.length, cfm.length);

  // initialization  
  foreach(i, ref row; distMat) {
    foreach(j, ref elem; row) {
      if(i == j) {
        elem = 0.0;
      }
      else {
        elem = cfm[i][j] == 0.0 ? double.infinity : 1.0 / cfm[i][j];
      }
    }
  }
  
  // iteration
  foreach(k; 0..cfm.length) 
    foreach(i; 0..cfm.length) 
      foreach(j; 0..cfm.length) 
        if(distMat[i][j] > distMat[i][k] + distMat[k][j]) 
          distMat[i][j] = distMat[i][k] + distMat[k][j];

  foreach(i, row; distMat) 
    foreach(j, elem; row) 
      if(distMat[i][j] == double.infinity) 
        throw new Exception("ERROR: multiple connected components");
        
  return distMat;
}


double[][] DtoG(double[][] distMat)
{
  immutable auto N = distMat.length;
  auto sqrDistMat = distMat.map!(x => x.map!(y => y^^2 / 2.0));

  auto sqrD = new double[](N);
  foreach(i; 0..N)
    sqrD[i] = sqrDistMat[i].sum / N;

  auto sqrAvrg = reduce!("a + b.sum")(0.0, sqrDistMat) / N / N;

  auto gramMat = new double[][](N, N);
  foreach(i; 0..N)
    foreach(j; 0..N)
      gramMat[i][j] = sqrD[i] + sqrD[j] - sqrAvrg - sqrDistMat[i][j];

  return gramMat;
}


Tuple!(double[], "eigVals", double[][], "eigVecs") GtoE(double[][] gramMat)
{
  char jobz = 'V';
  char uplo = 'U';
  integer n = gramMat.length;
  doublereal[] a = reduce!((a,b) => a ~ b)(new doublereal[](0), gramMat);
  integer lda = n;
  doublereal[] w = new doublereal[](n);
  doublereal* work = new doublereal(1);
  integer lwork = -1;
  integer info = 0;

  syev(jobz, uplo, n, a.ptr, lda, w.ptr, work, lwork, info);

  if(info != 0) {
    throw new
      Exception("ERROR: fail to calculate the optimal size of work array.");
  }
  
  lwork = work[0].to!integer;
  work = new double(lwork);
  syev(jobz, uplo, n, a.ptr, lda, w.ptr, work, lwork, info);

  if(info != 0) {
    throw new
      Exception("ERROR: fail to calculate the eigenvalues.");
  }

  double[] eigVals = w.map!(x => x.to!double).array;
  double[][] eigVecs = new double[][](n, n);
  foreach(i; 0..n) {
    foreach(j; 0..n) {
      eigVecs[i][j] = a[i + j * lda].to!double;
    }
  }
  
  return Tuple!(double[], "eigVals", double[][], "eigVecs")(eigVals, eigVecs);
}


// assuming eigVals is sorted by ascending order.
double[][] EtoX(double[] eigVals, double[][] eigVecs, double scalingFactor)
{
  if(eigVals.length < 3)
    throw new Exception("ERROR: the number of eigenvalues < 3");
  
  auto eigVal1 = eigVals[$-1];
  auto eigVal2 = eigVals[$-2];
  auto eigVal3 = eigVals[$-3];

  if(eigVal1 < 0 || eigVal1 < 0 || eigVal3 < 0)
    throw new Exception("ERROR: one of top 3 eigenvalues are negative.");
  
  auto eigVec1 = eigVecs.map!(x => x[$-1]).array.standardize(scalingFactor);
  auto eigVec2 = eigVecs.map!(x => x[$-2]).array.standardize(scalingFactor);
  auto eigVec3 = eigVecs.map!(x => x[$-3]).array.standardize(scalingFactor);
  
  double[][] coords;
  
  foreach(i; 0..eigVec1.length) {
    coords ~= [sqrt(eigVal1) * eigVec1[i],
               sqrt(eigVal2) * eigVec2[i],
               sqrt(eigVal3) * eigVec3[i]];
  }
  
  return coords;
}


double[] standardize(double[] v, double scaleFactor)
{
  return v[].map!(x => scaleFactor * x / v.euclideanNorm).array;
}


double calcGofEigVec(double[][] coords, double[][] distMat)
{
  double sqrSum = 0.0;
  
  foreach(i, row; distMat) 
    foreach(j, elem; row) 
      sqrSum += (euclideanDist(coords[i], coords[j]) - distMat[i][j])^^2;

  return sqrSum.sqrt / distMat.length^^2;
}


double euclideanDist(double[] u, double[] v)
{
  return zip(u, v).map!(x => (x[0] - x[1])^^2).sum.sqrt;
}


double euclideanNorm(double[] v)
{
  return v.map!(x => x^^2).sum;
}


double[][] readMat(string filename)
{
  auto matApp = appender!(double[][]);
  auto fin = File(filename, "r");
  foreach(line; fin.byLine) {
    matApp.put(line.to!string.strip.split("\t").map!(x => x.to!double).array);
  }
  return matApp.data;
}


void writeMat(double[][] mat, string filename)
{
  auto fout = File(filename, "w");
  foreach(row; mat) {
    foreach(i, elem; row) {
      fout.write(elem);
      //fout.write(format("%f", elem));
      if(i < row.length - 1) {
        fout.write("\t");
      }
    }
    fout.writeln;
  }
}


void writeVec(double[] mat, string filename)
{
  auto fout = File(filename, "w");
  foreach(elem; mat) {
    fout.writeln(elem);
    //fout.writeln(format("%f", elem));
  }
}
