import sys
import networkx as nx

args = sys.argv
inFile = args[1]


cfm = []
for line in open(inFile, 'r'):
    cfm.append([float(x) for x in line.rstrip().split('\t')])

    
G = nx.Graph()
G.add_nodes_from(range(len(cfm)))
for i in range(len(cfm)):
    for j in range(len(cfm)):
        if i != j and cfm[i][j] > 0:
            G.add_edge(i, j, weight = 1 / cfm[i][j])


print nx.floyd_warshall(G)
