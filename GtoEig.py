import sys, scipy, numpy


args = sys.argv
inFile = args[1]
outFile = args[2]


gramMat = []
for line in open(inFile, 'r'):
    gramMat.append([float(x) for x in line.rstrip().split('\t')])


G = numpy.array(gramMat)
v = numpy.linalg.eigvalsh(G)    

print v
