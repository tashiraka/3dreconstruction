#!/bin/sh

#prefix=cfm12.normalized
prefix=cfm.norm.binned

#./checkConnection $prefix $prefix.connected $prefix.connectivity
#./CtoD $prefix.connected $prefix.distMat
#./check_triangle_ineq $prefix.distMat
./DtoG_integer $prefix.distMat $prefix.gramMat_int
./DtoG_integer_to_double $prefix.distMat $prefix.gramMat_double
./GtoEig $prefix.gramMat_int $prefix.eigVal_int $prefix.eigVec_int
./GtoEig $prefix.gramMat_double $prefix.eigVal_double $prefix.eigVec_double
#./EigToX $prefix.eigVal $prefix.eigVec 0.001 $prefix.xyz
#./checkGOF $prefix.eigVal $prefix.xyz $prefix.distMat $prefix.gof
#./xyz_to_pdb $prefix.xyz $prefix.connectivity sacCer3.binInfo $prefix.pdb \
#&& rm $prefix.connected $prefix.connectivity $prefix.gramMat $prefix.eigVal $prefix.eigVec
