import std.stdio, std.string, std.conv, std.algorithm, std.array, std.math;

version(unittest) {}
 else {
   void main(string[] args)
   {
      auto inFile = args[1];
      
      auto mat = readMat(inFile);
      auto norm = mat.frobeniusNorm();
      writeln(norm);
   }
 }


double frobeniusNorm(double[][] mat)
{
   double norm = 0.0;
    
   foreach(row; mat) {
      foreach(elem; row) {
        norm += elem * elem;
      }
   }
   
   return norm.sqrt;
}


double[][] readMat(string filename)
{
  auto matApp = appender!(double[][]);
  auto fin = File(filename, "r");
  foreach(line; fin.byLine) {
    matApp.put(line.to!string.strip.split("\t").map!(x => x.to!double).array);
  }
  return matApp.data;
}
