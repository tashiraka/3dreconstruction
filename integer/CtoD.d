import std.stdio, std.conv, std.string, std.array, std.algorithm;


version(unittest){}
 else {
   void main(string[] args)
   {
     auto cfmFile = args[1];
     auto outFile = args[2];

     auto cfm = readMat(cfmFile);     
     auto distMat = cfmToDistMat(cfm);
     writeMat(distMat, outFile);
   }
 }


// Warshall-Floyd Algorithm
long[][] cfmToDistMat(double[][] cfm)
{
  auto distMat = new long[][](cfm.length, cfm.length);

  // initialization
  // Let the max of contact frequency c, the min of dist is 1 / c.
  // The max dist is 1.
  long inf = long.max / 100;
  double maxCF = 0.0;
  double minCF = double.infinity;
  foreach(i; 0..cfm.length) {
    foreach(j; (i+1)..cfm.length) {
      if(cfm[i][j] != 0) {
        maxCF = max(maxCF, cfm[i][j]);
        minCF = min(minCF, cfm[i][j]);
      }
    }
  }

  writeln("minCF: ", minCF);
  writeln("maxCF: ", maxCF);
  
  if(maxCF / minCF > inf) {
    throw new Exception("Number of digits between maxDist and minDist are too distinct. minDist: " ~ minCF.to!string ~ ", maxDist: " ~ maxCF.to!string);
  }

  double digitShifter = maxCF;

  writeln("digitShifter: ", digitShifter);
	writeln("ideal max distance: ", digitShifter / minCF);
  writeln("inf: ", inf);
  
  foreach(i, ref row; distMat) 
    foreach(j, ref elem; row) {
      if(i == j) {
        elem = 0;
      } else {
        elem = cfm[i][j] == 0.0 ? inf : (digitShifter / cfm[i][j]).to!long;
      }
    }

  // iteration
  foreach(k; 0..cfm.length) 
    foreach(i; 0..cfm.length) 
      foreach(j; 0..i) 
        if(distMat[i][j] > distMat[i][k] + distMat[k][j]) {
          distMat[i][j] = distMat[i][k] + distMat[k][j];
          distMat[j][i] = distMat[i][j];
        }

  foreach(i, row; distMat) 
    foreach(j, elem; row) 
      if(distMat[i][j] == inf) 
        throw new Exception("ERROR: multiple connected components");

	auto maxDist = long.min;
	auto minDist = long.max;
	foreach(i; 0..cfm.length) {
    foreach(j; (i+1)..cfm.length) {
			maxDist = distMat[i][j] > maxDist ? distMat[i][j] : maxDist;
			minDist = distMat[i][j] < minDist ? distMat[i][j] : minDist;			
		}
	}
  writeln("final minDist: ", minDist);
	writeln("final maxDist: ", maxDist);

  
  return distMat;
}


double[][] readMat(string filename)
{
  auto matApp = appender!(double[][]);
  auto fin = File(filename, "r");
  foreach(line; fin.byLine) {
    matApp.put(line.to!string.strip.split("\t").map!(x => x.to!double).array);
  }
  return matApp.data;
}


void writeMat(long[][] mat, string filename)
{
  auto fout = File(filename, "w");
  foreach(row; mat) {
    foreach(i, elem; row) {
      fout.write(elem);
      //      fout.write(format("%f", elem));
      if(i < row.length - 1) {
        fout.write("\t");
      }
    }
    fout.writeln;
  }
}
