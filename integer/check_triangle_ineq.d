import std.stdio, std.conv, std.string, std.array, std.algorithm, std.math;


version(unittest){}
 else {
   void main(string[] args)
   {
     auto distFile = args[1];

     auto distMat = readMat(distFile);
     
     foreach(i; 0..distMat.length){
       foreach(j; (i+1)..distMat.length){
         foreach(k; 0..distMat.length){
           if(i != k && j != k) {
             auto a = distMat[i][j];
             auto b = distMat[j][k];
             auto c = distMat[k][i];
             if(!(c - abs(a - b) >= 0) 
                || !((a + b) - c >= 0)) {
               writeln([i, j, k]);
               writeln([a, b, c]);
               writeln(abs(a - b));
               writeln(c - abs(a - b));
               writeln(c - abs(a - b) >= 0);
               writeln(a + b);
               writeln((a + b) - c);
               writeln((a + b) - c >= 0);
               throw new Exception("ERROR: violating triangle inequation");
             }
           }
         }
       }
     }
   }
 }

long[][] readMat(string filename)
{
  auto matApp = appender!(long[][]);
  auto fin = File(filename, "r");
  foreach(line; fin.byLine) {
    matApp.put(line.to!string.strip.split("\t").map!(x => x.to!long).array);
  }
  return matApp.data;
}
