import std.stdio, std.conv, std.string, std.array, std.algorithm, std.math;


version(unittest){}
 else {
   void main(string[] args)
   {
     auto distMatFile = args[1];
     auto outFile = args[2];

     auto distMat = readMat(distMatFile);
     auto gramMat = DtoG(distMat);
     writeMat(gramMat, outFile);
   }
 }


double[][] DtoG(long[][] distMat)
//long[][] DtoG(long[][] distMat)
{
  immutable long N = distMat.length;
  writeln("N: ", N);
  writeln("N^2: ", N * N);

  foreach(i; 0..N) {
    if(distMat[i][i] != 0) {
      throw new Exception("Diagonals of distMat are not zero.");
    }
  }
  
  auto sqrDistMat = new long[][](N, N);
  foreach(i; 0..N) {
    foreach(j; i..N) {
      sqrDistMat[i][j] = distMat[i][j] * distMat[i][j];
    }
  }
  //auto sqrDistMat = distMat.map!(x => x.map!(y => y^^2));

  auto sqrD = new long[](N);
  foreach(i; 0..N) {
    sqrD[i] = sqrDistMat[i].sum;
  }

  auto sqrAvrg = sqrD.sum;
  //auto sqrAvrg = reduce!("a + b.sum")(long(0), sqrDistMat);

  writeln("inf: ", long.max);
  writeln("sqrAvrg: ", sqrAvrg);

  auto max = long.min;
  foreach(i; 0..N) {
    max = max > sqrD[i] ? max : sqrD[i];
  }

  writeln("max sqrD: ", max * N);

  max = long.min;
  foreach(i; 0..N) {
    foreach(j; 0..N) {
      max = max > sqrDistMat[i][j] ? max : sqrDistMat[i][j];
    }
  }
  writeln("max sqrDistMat: ", max * N * N);
  writeln("dliminator: ",  (N * N * 2 *  51664.3 * 51664.3));

  auto gramMat = new double[][](N, N);
  //auto gramMat = new long[][](N, N);
  foreach(i; 0..N) {
    foreach(j; i..N) {
      auto tmp = (sqrD[i] * N + sqrD[j] * N - sqrAvrg * 2 - sqrDistMat[i][j] * N * N);
			gramMat[i][j] = tmp;
      gramMat[i][j] = tmp.to!double / (N * N * 2 *  51664.3 * 51664.3).to!double;
      gramMat[j][i] = gramMat[i][j];
      
      if(tmp < - long(2)^^52 || long(2)^^52 < tmp) { // for fitting to double
        throw new Exception("A value of gramMat is over the fraction of double.\n" ~ 
                            "gramMat: " ~ tmp.to!string ~ "\n" ~
                            "52 bit: " ~ (long(2)^^52).to!string);
      }
    }
  }
      
  return gramMat;
}


long[][] readMat(string filename)
{
  auto matApp = appender!(long[][]);
  auto fin = File(filename, "r");
  foreach(line; fin.byLine) {
    matApp.put(line.to!string.strip.split("\t").map!(x => x.to!long).array);
  }
  return matApp.data;
}


void writeMat(T)(T[][] mat, string filename)
{
  auto fout = File(filename, "w");
  foreach(row; mat) {
    foreach(i, elem; row) {
      fout.write(elem);
      //fout.write(format("%f", elem));
      if(i < row.length - 1) {
        fout.write("\t");
      }
    }
    fout.writeln;
  }
}
