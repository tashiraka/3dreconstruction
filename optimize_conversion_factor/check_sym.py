import math, sys

args = sys.argv

mat = []

for line in open(args[1], 'r'):
    mat.append([float(x) for x in line.rstrip().split('\t')])

size = len(mat)
for i in range(size):
    for j in range(size):
        if(mat[i][j] != mat[j][i]):
            print "ERROR: not symmetric " + str([i, j])
            quit()
