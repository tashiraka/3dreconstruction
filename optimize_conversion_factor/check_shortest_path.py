import sys

args = sys.argv

distMat = []
for line in open(args[1], 'r'):
    distMat.append([float(x) for x in line.rstrip().split('\t')])


#i = int(args[2])
#j = int(args[3])
#dist = []
#for k in range(len(distMat)):
#    dist += [distMat[i][k] + distMat[k][j]]

#print 'calculated shortest path:'
#print distMat[i][j]
#print 'verified shortest path:'
#print min(dist)
#print dist.index(min(dist))


for i in range(len(distMat)):
    for j in range(len(distMat)):
        dist = []
        for k in range(len(distMat)):
            dist += [distMat[i][k] + distMat[k][j]]
        minDist = min(dist)
        print minDist - distMat[i][j]    
