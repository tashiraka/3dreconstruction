import sys, math

args = sys.argv

gramMat = []

for line in open(args[1], 'r'):
    gramMat.append([float(x) for x in line.rstrip().split('\t')])

size = len(gramMat)
v = [math.exp(-i) for i in range(size)]    

qForm = 0.0
for i in range(size):
    for j in range(size):
        qForm += gramMat[i][j] * v[i] * v[j]

print qForm
