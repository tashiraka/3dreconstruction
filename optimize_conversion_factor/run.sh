#!/bin/sh

./checkConnection cfm.norm.binned cfm.connected bin.connectivity
./CtoD cfm.connected 1 distMat
./DtoG distMat gramMat
#./estimate_structure cfm.connected 1 gramMat
./GtoEig gramMat eigVal eigVec
./EigToX eigVal eigVec 3Dcoord.xyz
./checkGOF eigVal 3Dcoord.xyz distMat gof.txt
./xyz_to_pdb 3Dcoord.xyz bin.connectivity chrIntervals.txt 3Dcoord.pdb

#./XtoD 3Dcoord.xyz distMat_recover

#./DtoG distMat_recover gramMat_recover
#./GtoEig gramMat_recover eigVal_recover eigVec_recover
#./EigToX eigVal_recover eigVec_recover 3Dcoord_recover.xyz
#./checkGOF eigVal_recover 3Dcoord_recover.xyz distMat_recover gof_recover.txt


#./checkConnection test test.connected test.bin.connectivity
#./CtoD test.connected 1 test.distMat
#./DtoG test.distMat test.gramMat
#./GtoEig test.gramMat test.eigVal test.eigVec
#./EigToX test.eigVal test.eigVec test.3Dcoord.xyz
#./checkGOF test.eigVal test.xyz test.distMat test.gof
