import std.stdio, std.conv, std.string, std.array, std.algorithm, std.math;


version(unittest){}
 else {
   void main(string[] args)
   {
     auto cfmFile = args[1];
     auto convFacter = args[2].to!double;
     auto outFile = args[3];

     auto cfm = readMat(cfmFile);     
     auto distMat = cfmToDistMat(cfm, convFacter);
     writeMat(distMat, outFile);
   }
 }


int digit(double d)
{
  if(d == 0) {
    return 0;
  }
  if(d > 1) {
    auto digit = 0;
    auto x = d.abs.to!int;
    
    while(x != 0) {
      x /= 10;
      digit += 1;
    }

    return digit;
  }
  else {
    auto digit = 0;
    auto x = d.abs.to!int;
    
    while(x == 0) {
      d *= 10;
      x = d.abs.to!int;
      digit -= 1;
    }

    return digit;
  }
}


int digit(ulong d)
{
  auto digit = 0;
  
  while(d != 0) {
    d /= 10;
    digit += 1;
  }
  
  return digit;
}


// Warshall-Floyd Algorithm
/*
double[][] cfmToDistMat(double[][] cfm, double convFacter)
{
  auto distMat = new double[][](cfm.length, cfm.length);

  // initialization
  foreach(i, ref row; distMat) 
    foreach(j, ref elem; row)
      if(i == j)
        elem = 0;
      else
        elem = (cfm[i][j] == 0
                ? double.infinity : 1.0 / (cfm[i][j]^^convFacter));
  
  // iteration
  foreach(k; 0..cfm.length) 
    foreach(i; 0..cfm.length) 
      foreach(j; 0..cfm.length)
        distMat[i][j] = min(distMat[i][j], distMat[i][k] + distMat[k][j]);

  foreach(i, row; distMat) 
    foreach(j, elem; row) 
      if(distMat[i][j] == double.infinity)
        throw new Exception("ERROR: multiple connected components");
  
  return distMat;
}
*/

double[][] cfmToDistMat(double[][] cfm, double convFacter)
{
  writeln("initialization");
  auto distMat = new double[][](cfm.length, cfm.length);
  // initialization
  foreach(i, ref row; distMat) 
    foreach(j, ref elem; row)
      if(i == j)
        elem = 0;
      else
        elem = (cfm[i][j] == 0
                ? double.infinity : 1.0 / (cfm[i][j] ^^ convFacter));

  writeln("check digit");
  auto maxElem = - double.infinity;
  auto minElem = double.infinity;
  foreach(row; distMat)
    foreach(elem; row)
      if(elem != 0 && elem != double.infinity) {
        maxElem = max(maxElem, elem);
        minElem = min(minElem, elem);
      }

  //16 digit precision
  auto maxDigit = maxElem.digit;
  auto minDigit = minElem.digit;
  if(maxDigit - minDigit + 16 > 900) {
    throw new Exception("ERROR: the difference of digits of elemens is too large.");
  }

  auto maxUlongDigit = ulong.max.digit;  
  auto digitShift = 10.0 ^^ (maxUlongDigit - maxDigit - 2); //-2 is for overflow
  writeln(maxDigit);
  writeln(minDigit);
  writeln(maxUlongDigit);
  writeln(minDigit + maxUlongDigit - maxDigit - 2);
  
  writeln("create ulongMatrix");
  auto distMatInt = new ulong[][](cfm.length, cfm.length);

  foreach(i, ref row; distMatInt) {
    foreach(j, ref elem; row) {
      if(i == j) {
        elem = 0;
      }
      else {
        elem = (cfm[i][j] == 0
                ? ulong.max
                : (1.0 / (cfm[i][j] ^^ convFacter) * digitShift).to!ulong);
      }
    }
  }

  writeln("search shortest path");
  // iteration
  foreach(k; 0..cfm.length) 
    foreach(i; 0..cfm.length) 
      foreach(j; 0..cfm.length)
        distMatInt[i][j] = min(distMatInt[i][j],
                               distMatInt[i][k] + distMatInt[k][j]);

  foreach(i, row; distMatInt) 
    foreach(j, elem; row) 
      if(distMatInt[i][j] == ulong.max)
        throw new Exception("ERROR: multiple connected components.");

  writeln("ulong to double");
  foreach(j, row; distMatInt) { 
    foreach(i, elem; row) {
      distMat[i][j] = elem.to!double / digitShift;
      if(distMat[i][j] == double.nan) {
        writeln(elem);
        throw new Exception("nannanananannananananana");
      }
    }
  }

  return distMat;
}


double[][] readMat(string filename)
{
  auto matApp = appender!(double[][]);
  auto fin = File(filename, "r");
  foreach(line; fin.byLine) {
    matApp.put(line.to!string.strip.split("\t").map!(x => x.to!double).array);
  }
  return matApp.data;
}


void writeMat(double[][] mat, string filename)
{
  auto fout = File(filename, "w");
  foreach(row; mat) {
    foreach(i, elem; row) {
      fout.write(format("%f", elem));
      if(i < row.length - 1) {
        fout.write("\t");
      }
    }
    fout.writeln;
  }
}
