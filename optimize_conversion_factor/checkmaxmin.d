import std.stdio, std.conv, std.string, std.array, std.algorithm, std.math;


void main(string[] args)
{
  auto cfmFile = args[1];
  auto convFacter = args[2].to!double;
  
  auto cfm = readMat(cfmFile);
  auto distMat = new double[][](cfm.length, cfm.length);
  
  foreach(i, ref row; distMat) 
    foreach(j, ref elem; row)
      if(i == j)
        elem = 0;
      else
        elem = (cfm[i][j] == 0
                ? 0 : 1.0 / (cfm[i][j]^^convFacter));

  auto max = reduce!((a, b) => max(a, reduce!(max)(b)))(0.0, distMat);

  foreach(i, ref row; distMat) 
    foreach(j, ref elem; row)
      if(i == j)
        elem = double.infinity;
      else
        elem = (cfm[i][j] == 0
                ? double.infinity : 1.0 / (cfm[i][j]^^convFacter));

  auto min = reduce!((a, b) => min(a, reduce!(min)(b)))(double.infinity, distMat);

  writeln(max);
  writeln(min);
}


double[][] readMat(string filename)
{
  auto matApp = appender!(double[][]);
  auto fin = File(filename, "r");
  foreach(line; fin.byLine) {
    matApp.put(line.to!string.strip.split("\t").map!(x => x.to!double).array);
  }
  return matApp.data;
}
