import sys
import networkx as nx

args = sys.argv


cfm = []
for line in open(args[1], 'r'):
    cfm.append([float(x) for x in line.rstrip().split('\t')])

    
G = nx.Graph()
G.add_nodes_from(range(len(cfm)))
for i in range(len(cfm)):
    for j in range(len(cfm)):
        if i != j and cfm[i][j] > 0:
            G.add_edge(i, j)


components = nx.components.number_connected_components(G)

print components            
