import std.stdio, std.conv, std.string, std.array, std.algorithm, std.math;


version(unittest){}
 else {
   void main(string[] args)
   {
     auto cfmFile = args[1];
     auto convFacter = args[2].to!double;
     auto outFile = args[3];

     auto cfm = readMat(cfmFile);
     auto distMat = cfmToDistMat(cfm, convFacter);
     auto gramMat = DtoG(distMat);
     gramMat = gramMat.scaling(100, 1000);
     writeMat(gramMat, outFile);
   }
 }


double[][] scaling(double[][] gramMat, double max, double min)
{
  auto maxElem = 0.0;
  foreach(row; gramMat)
    foreach(elem; row)
      maxElem = elem.abs > maxElem ? elem.abs : maxElem;

  auto digitChange = 1.0;
  while(maxElem >= max) {
    maxElem /= 10;
    digitChange /= 10;
  }
  while(maxElem < min) {
    maxElem *= 10;
    digitChange *= 10;
  }
  
  return gramMat.map!(x => x.map!(y => y * digitChange).array).array;
}


double[][] DtoG(double[][] distMat)
{
  immutable auto N = distMat.length;
  auto gramMat = new double[][](N, N);
  auto sqrD0 = new double[](N);
  auto sqrDistMat = distMat.map!(x => x.map!(y => y^^2));
  auto sqrAvrg = reduce!("a + b.sum")(0.0, sqrDistMat) / (2.0 * N^^2);
  
  foreach(i; 0..N)
    sqrD0[i] = sqrDistMat[i].sum / N - sqrAvrg;

  foreach(i; 0..N)
    foreach(j; 0..N)
      gramMat[i][j] = (sqrD0[i] + sqrD0[j] - sqrDistMat[i][j]) / 2.0;

  return gramMat;
}


// Warshall-Floyd Algorithm
double[][] cfmToDistMat(double[][] cfm, double convFacter)
{
  auto distMat = new double[][](cfm.length, cfm.length);

  // initialization
  foreach(i, ref row; distMat) 
    foreach(j, ref elem; row)
      if(i == j)
        elem = 0.0;
      else
        elem = (cfm[i][j] == 0.0
                ? double.infinity : 1.0 / (cfm[i][j]^^convFacter));
  
  // iteration
  foreach(k; 0..cfm.length) 
    foreach(i; 0..cfm.length) 
      foreach(j; 0..cfm.length) 
        if(distMat[i][j] > distMat[i][k] + distMat[k][j]) 
          distMat[i][j] = distMat[i][k] + distMat[k][j];

  foreach(i, row; distMat) 
    foreach(j, elem; row) 
      if(distMat[i][j] == double.infinity) 
        throw new Exception("ERROR: multiple connected components");
  
  return distMat;
}


double[][] readMat(string filename)
{
  auto matApp = appender!(double[][]);
  auto fin = File(filename, "r");
  foreach(line; fin.byLine) {
    matApp.put(line.to!string.strip.split("\t").map!(x => x.to!double).array);
  }
  return matApp.data;
}


void writeMat(double[][] mat, string filename)
{
  auto fout = File(filename, "w");
  foreach(row; mat) {
    foreach(i, elem; row) {
      fout.write(format("%.16f", elem));
      if(i < row.length - 1) {
        fout.write("\t");
      }
    }
    fout.writeln;
  }
}
