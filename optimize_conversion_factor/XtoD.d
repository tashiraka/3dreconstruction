import std.stdio, std.conv, std.string, std.array, std.algorithm, std.math, std.range;


version(unittest){}
 else {
   void main(string[] args)
   {
     auto distMatFile = args[1];
     auto outFile = args[2];

     auto coordMat = readMat(distMatFile);
     auto distMat = XtoD(coordMat);
     writeMat(distMat, outFile);
   }
 }


double[][] XtoD(double[][] X)
{
  double[][] distMat;
  foreach(i, x1; X) {
    double[] row;
    foreach(j, x2; X) {
      row ~= euclidDistance(x1, x2);
    }
    distMat ~= row.dup;
  }
  return distMat;
}


double euclidDistance(double[] u, double[] v)
{
  return reduce!((a, b) => a + (b[0] - b[1])^^2)(0.0, zip(u, v)).sqrt;
}


double[][] readMat(string filename)
{
  auto matApp = appender!(double[][]);
  auto fin = File(filename, "r");
  foreach(line; fin.byLine) {
    matApp.put(line.to!string.strip.split("\t").map!(x => x.to!double).array);
  }
  return matApp.data;
}


void writeMat(double[][] mat, string filename)
{
  auto fout = File(filename, "w");
  foreach(row; mat) {
    foreach(i, elem; row) {
      fout.write(elem);
      if(i < row.length - 1) {
        fout.write("\t");
      }
    }
    fout.writeln;
  }
}
