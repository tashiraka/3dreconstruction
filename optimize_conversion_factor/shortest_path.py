import sys
import networkx as nx


args = sys.argv
inFile = args[1]
convFacter = float(args[2])
outFile = args[3]

cfm = []
for line in open(inFile, 'r'):
    cfm.append([float(x) for x in line.rstrip().split('\t')])


G = nx.Graph()
G.add_nodes_from(range(len(cfm)))
for i in range(len(cfm)):
    for j in range(len(cfm)):
        if i != j and cfm[i][j] > 0:
            G.add_edge(i, j, weight = 1.0 / (cfm[i][j] ** convFacter))


sp = nx.algorithms.shortest_paths.dense.floyd_warshall(G)


fout = open(outFile, 'w')
for i in range(len(cfm)):
    for j in range(len(cfm)):
        fout.write(sp[i][j])
        if j < range(len(cfm)) - 1:
            fout.write('\t')
    fout.write('\n')
