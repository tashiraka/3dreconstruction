import os, math


def readGOF():
    for line in open('gof.txt', 'r'):
        if line[0:4] == 'top3':
            return float(line.rstrip().split(' ')[-1])

def readConsensusIndex():
    for line in open('gof.txt', 'r'):
        if line[0] == 'c':
            return float(line.rstrip().split(' ')[-1])
            

def mainProcess(convFacter):
    os.system('./CtoD cfm.connected ' + str(convFacter) + ' distMat')
    os.system('./DtoG distMat gramMat')
    os.system('./GtoEig gramMat eigVal eigVec')
    os.system('./EigToX eigVal eigVec 3Dcoord.xyz')
    os.system('./checkGOF eigVal 3Dcoord.xyz distMat gof.txt')
    os.system('echo "conversion facter: ' + str(convFacter) + '" >> gof.txt')
    return readConsensusIndex()

    
def goldenSectionSearch(f, l, h, eps, maxLoop):
    phi = (1.0 + math.sqrt(5.0)) / 2.0
    lo = l
    hi = h
    mid = (hi - lo) / (1.0 + phi)
    newPoint = 0.0
    loopNum = 0
    tau = math.sqrt(eps)
    
    loVal = f(math.exp(lo))
    midVal = f(math.exp(mid))
    hiVal = f(math.exp(hi))
    
    if midVal < loVal or midVal < hiVal:
        print 'ERROR: not unimodal.'
        quit()

    print "Threshold: " + str(tau)
    print math.fabs(hi - lo) / (math.fabs(mid) + math.fabs(newPoint))
    print "GOF: " + str(midVal)
    print "hi: " + str(hi)
    print "lo: " + str(lo)
    
    while math.fabs(hi - lo) >= tau * (math.fabs(mid) + math.fabs(newPoint)) \
          and loopNum < maxLoop:
        newPoint = (phi * hi + lo) / (1.0 + phi)
        newVal = f(math.exp(newPoint))

        if midVal < newVal:
            lo = mid
            mid = newPoint
            hi = hi

            loVal = midVal
            midVal = newVal
            hiVal = hiVal
        else:
            lo = lo
            mid = mid
            hi = newPoint

            loVal = loVal
            midVal = midVal
            hiVal = newVal

        if midVal < loVal or midVal < hiVal:
            print 'ERROR: not unimodal.'
            quit()

            
        loopNum += 1
        print math.fabs(hi - lo) / (math.fabs(mid) + math.fabs(newPoint))
        print "GOF of mid: " + str(midVal)
        print "hi: " + str(hi)
        print "lo: " + str(lo)

    return (hi + lo) / 2.0


if __name__ == '__main__':
    lowerBound = math.log(0.1)
    upperBound = math.log(3)
    precision = 0.0000000001
    maxLoop = 30

    os.system('./checkConnection cfm.norm.binned cfm.connected ' \
              + 'bin.connectivity')
    optLogAlpha = goldenSectionSearch(mainProcess, lowerBound, \
                                      upperBound, precision, maxLoop)
    mainProcess(math.exp(optLogAlpha))
    os.system('./xyz_to_pdb 3Dcoord.xyz bin.connectivity ' \
              + 'chrIntervals.txt 3Dcoord.pdb')
        
