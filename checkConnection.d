import std.stdio, std.conv, std.string, std.algorithm, std.array;


version(unittest){}
 else{
   void main(string[] args)
   {
     auto cfmFile = args[1];
     auto outCfmFile = args[2];
     auto outBinFile = args[3];

     auto binFout = File(outBinFile, "w");

     string[][] cfm;
     bool[] connectedFlag;
     int lineNum= 0;
     
     foreach(line; File(cfmFile, "r").byLine) {
       auto fields = line.to!string.strip.split("\t");
       cfm ~= fields;
       auto c = fields.map!(x => x.to!double).sum;

       if(c == 0) {
         connectedFlag ~= false;
         binFout.writeln(lineNum, "\tnot_connected");
       }
       else {
         connectedFlag ~= true;
         binFout.writeln(lineNum, "\tconnected");
       }
       
       lineNum++;
     }

     auto cfmFout = File(outCfmFile, "w");
     
     foreach(i, row; cfm) {
       if(connectedFlag[i]) {
         foreach(j, elem; row) {
           if(connectedFlag[j]) {
             cfmFout.write(elem, "\t");
           }
         }
         cfmFout.writeln;
       }
     }
   }
 }
