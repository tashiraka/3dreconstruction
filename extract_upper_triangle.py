import sys


args = sys.argv
matFile = args[1]
outFile = args[2]


mat = []
for line in open(matFile, 'r'):
    fields = line.rstrip().split('\t')
    mat.append(fields)


for i in range(0, len(mat)):
    for j in range(i, len(mat)):
        mat[i][j] = mat[j][i]


fout = open(outFile, 'w')
for row in mat:
    for i, elem in enumerate(row):
        fout.write(elem)
        if i < len(row) - 1:
            fout.write('\t')
    fout.write('\n')
