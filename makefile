DMD = dmd
RDMD = rdmd
DFLAGS = -O -release -inline -noboundscheck
TESTDFLAGS = -unittest -main
DSRC = $(wildcard *.d)
DOBJ = $(DSRC:%.d=%.o)
DEXE = $(DSRC:%.d=%)

GCC = gcc
CFLAGS = -O3
CLAPACKFLAGS = -I/home/yuichi/opt/CLAPACK-3.2.1/yuichi_installed/include -L/home/yuichi/opt/CLAPACK-3.2.1/yuichi_installed/lib -I/home/yuichi/opt/ATLAS-3.10.2/yuichi_installed/include -L/home/yuichi/opt/ATLAS-3.10.2/yuichi_installed/lib
CSRC = $(wildcard *.c)
COBJ = $(CSRC:%.c=%.o)
CEXE = $(CSRC:%.c=%)

.PHONY: all clean test

all: $(DEXE) $(CEXE)

checkConnection: checkConnection.d
	$(DMD) $(DFLAGS) $^

CtoD: CtoD.d
	$(DMD) $(DFLAGS) $^

DtoG: DtoG.d
	$(DMD) $(DFLAGS) $^

GtoEig: GtoEig.c
	$(GCC) $(CFLAGS) $(CLAPACKFLAGS) $^ -lgsl -lclapack_LINUX -lctmglib_LINUX -L/usr/atlas/lib -lptcblas -lptf77blas -latlas -lf2c -lpthread -lm -o $@

EigToX: EigToX.d
	$(DMD) $(DFLAGS) $^

checkGOF: checkGOF.d
	$(DMD) $(DFLAGS) $^

xyz_to_pdb: xyz_to_pdb.d
	$(DMD) $(DFLAGS) $^

test:
	$(RDMD) $(TESTDFLAGS) $(DSRC)

clean:
	rm *~ $(DOBJ) $(DEXE) $(COBJ) $(CEXE)
