gramMat = []

for line in open('gramMat', 'r'):
    gramMat.append([float(x) for x in line.rstrip().split('\t')])

size = len(gramMat)
v = [i * 0.01 for i in range(size)]    

qForm = 0.0
for i in range(size):
    for j in range(size):
        qForm += gramMat[i][j] * v[i] * v[j]

print qForm
