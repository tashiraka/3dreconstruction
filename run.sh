#!/bin/sh

#prefix=cfm12.normalized
prefix=test_heatmap

./checkConnection $prefix.csv $prefix.connected $prefix.connectivity
./CtoD $prefix.connected $prefix.distMat
./DtoG $prefix.distMat $prefix.gramMat
./GtoEig $prefix.gramMat $prefix.eigVal $prefix.eigVec
./EigToX $prefix.eigVal $prefix.eigVec 1 $prefix.xyz
./checkGOF $prefix.eigVal $prefix.xyz $prefix.distMat $prefix.gof
./xyz_to_pdb $prefix.xyz $prefix.connectivity chrIntervalsTest.txt $prefix.pdb \
&& rm $prefix.connected $prefix.connectivity $prefix.gramMat $prefix.eigVal $prefix.eigVec

#cat data.dat | tr -s ' ' '\t' | sed 's/^\t//g' > data.format
#python extract_upper_triangle.py data.format data.format.sym
#./checkConnection data.format.sym data.format.connected data.bin.connectivity
#./CtoD data.format.connected data.distMat
#./DtoG data.distMat data.gramMat
#./GtoEig data.gramMat data.eigVal data.eigVec
#./EigToX data.eigVal data.eigVec 1 data.3Dcoord.xyz
#./checkGOF data.eigVal data.3Dcoord.xyz data.distMat data.gof.txt
#./xyz_to_pdb data.3Dcoord.xyz data.bin.connectivity data.chrIntervals.txt data.3Dcoord.pdb

#cat data2.dat | tr -s ' ' '\t' | sed 's/^\t//g' > data2.format
#./checkConnection data2.format data2.format.connected data2.bin.connectivity
#./CtoD data2.format.connected data2.distMat
#./DtoG data2.distMat data2.gramMat
#./GtoEig data2.gramMat data2.eigVal data2.eigVec
#./EigToX data2.eigVal data2.eigVec 1 data2.3Dcoord.xyz
#./checkGOF data2.eigVal data2.3Dcoord.xyz data2.distMat data2.gof.txt
